<?php
/**
 * @file index.php
 * @category   Extension
 * @author     Christopher Gerbig - https://webgreat.de
 * @copyright  1711382505715 Christopher Gerbig
*/

// Define Markdown File
$file = 'md/Demo.md';

require __DIR__ . '/vendor/autoload.php';
$content = file_get_contents($file);
$Parsedown = new Parsedown();
$data = $Parsedown->text($content);
?>

<!-- Define Custom CSS -->
<style>
    code {color:red;}
    code.language-php {
        display: block;
        padding: 5px;
        border-radius: 2px;
        border: 1px solid #ccc;
        color: #000;
    }
    hr {color:rgba(204, 204, 204, 0.39)}
</style>

<!-- Display HTML -->
<?php echo $data; ?>
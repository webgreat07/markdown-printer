# Simple Markdown Printer
This PHP code reads a Markdown file, converts it to HTML using an external library like "Parsedown," and outputs the HTML content. Subsequently, the HTML content is either displayed on the screen or printed. The Markdown file's filename is hardcoded in the code and can be adjusted accordingly.

Dieser PHP-Code liest eine Markdown-Datei ein, wandelt sie in HTML um und gibt den HTML-Inhalt aus. Es wird eine externe Bibliothek wie "Parsedown" verwendet, um das Markdown zu HTML zu konvertieren. Anschließend wird der HTML-Inhalt entweder auf dem Bildschirm angezeigt oder gedruckt. Der Dateiname der Markdown-Datei ist im Code festgelegt und kann entsprechend angepasst werden.

---

```
composer require erusev/parsedown
```

Define the Markdown File 
```
$file = 'md/Demo.md';
```

Add Custom CSS and Print over Browser Print Dialog.
